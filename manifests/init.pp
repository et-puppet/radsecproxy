# Class: radsecproxy
# ===========================
#
# configure an Emerging Technology Duo Proxy
#
# === Authors
#
# Scotty Logan <swl@stanford.edu>
#
# === Copyright
#
# Copyright (c) 2017 The Board of Trustees of the Leland Stanford Junior
# University
#
class radsecproxy {

  class { 'radsecproxy::packages': }

}

