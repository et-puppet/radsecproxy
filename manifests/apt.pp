# == Class: radsecproxy::apt
#
# APT configuration for radsecproxy systems
#
# === Authors
#
# Scotty Logan <swl@stanford.edu>
#
# === Copyright
#
# Copyright (c) 2017 The Board of Trustees of the Leland Stanford Junior University
#
class radsecproxy::apt {

  class { 'apt':
    purge   => {
      'sources.list'   => false,
      'sources.list.d' => false,
    },
  }

  # force apt-get update before package installation
  Class['apt::update'] -> Package<| |>

}
